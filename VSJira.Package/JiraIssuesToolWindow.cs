﻿using Microsoft.VisualStudio.Shell;
using System;
using System.Runtime.InteropServices;
using VSJira.Core.UI;

namespace VSJira.Package
{
    /// <summary>
    /// This class implements the tool window exposed by this package and hosts a user control.
    ///
    /// In Visual Studio tool windows are composed of a frame (implemented by the shell) and a pane,
    /// usually implemented by the package implementer.
    ///
    /// This class derives from the ToolWindowPane class provided from the MPF in order to use its
    /// implementation of the IVsUIElementPane interface.
    /// </summary>
    [Guid(GuidList.guidJiraIssuesToolWindow)]
    public class JiraIssuesToolWindow : ToolWindowPane
    {
        private readonly JiraIssuesUserControl _content;

        /// <summary>
        /// Standard constructor for the tool window.
        /// </summary>
        public JiraIssuesToolWindow() :
            base(null)
        {
            // Set the window title reading it from the resources.
            this.Caption = Resources.ToolWindowTitle;
            // Set the image that will appear on the tab of the window frame
            // when docked with an other window
            // The resource ID correspond to the one defined in the resx file
            // while the Index is the offset in the bitmap strip. Each image in
            // the strip being 16x16.
            this.BitmapResourceID = 301;
            this.BitmapIndex = 1;

            var viewModel = new JiraIssuesUserControlViewModel(VSJiraPackage.Services);
            base.Content = _content = new JiraIssuesUserControl(viewModel);
        }

        protected override void Dispose(bool disposing)
        {
            if (_content != null)
            {
                _content.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
