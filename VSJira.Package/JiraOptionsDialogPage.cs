﻿using Microsoft.VisualStudio.Settings;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Settings;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using VSJira.Core;

namespace VSJira.Package
{
    [ClassInterface(ClassInterfaceType.AutoDual)]
    [CLSCompliant(false), ComVisible(true)]
    public class JiraOptionsDialogPage : DialogPage, IJiraOptions
    {
        public const string CategoryName = "Atlassian";
        public const string PageName = "Jira";

        private const int DefaultMaxIssuesPerRequest = 100;
        private const string SettingsCollectionName = "VSJira";
        private const string SettingsPropertyName = "JiraSettings";
        private JsonSerializerSettings serializerSettings = new JsonSerializerSettings()
        {
            NullValueHandling = NullValueHandling.Ignore,
            MissingMemberHandling = MissingMemberHandling.Ignore
        };

        private VSJiraTheme _theme;
        private VSJiraOpenIssueMode _openIssueMode;

        [DisplayName("JIRA Servers")]
        [Description("The list of JIRA servers registered.")]
        public JiraServerInfo[] JiraServers { get; set; }

        [DisplayName("Items Per Request")]
        [Description("Maximum number of items of a resource type to retrieve per request.")]
        public int MaxIssuesPerRequest { get; set; }

        [DisplayName("Enable Request Tracing")]
        [Description("Whether to turn on request/response tracing. You must reconnect to the JIRA server for this setting to take effect. Only turn on when debugging issues with the server.")]
        public bool EnableRequestTracing { get; set; }

        [DisplayName("Open Issue Mode")]
        [Description("How to open issues when clicking on keys in issue lists.")]
        public VSJiraOpenIssueMode OpenIssueMode
        {
            get
            {
                return _openIssueMode;
            }
            set
            {
                _openIssueMode = value;
                this.OpenIssueModeHidden = value.ToString();
            }
        }

        [Browsable(false)]
        public string OpenIssueModeHidden { get; set; }

        [DisplayName("Theme")]
        [Description("Style to use for windows and dialogs.")]
        public VSJiraTheme Theme
        {
            get
            {
                return _theme;
            }
            set
            {
                _theme = value;
                this.ThemeHidden = value.ToString();
            }
        }

        [Browsable(false)]
        public string ThemeHidden { get; set; }

        public JiraOptionsDialogPage()
        {
            this.MaxIssuesPerRequest = DefaultMaxIssuesPerRequest;
            this.Theme = VSJiraTheme.Light;
        }

        public override void SaveSettingsToStorage()
        {
            base.SaveSettingsToStorage();

            var settingsManager = new ShellSettingsManager(ServiceProvider.GlobalProvider);
            var userSettingsStore = settingsManager.GetWritableSettingsStore(SettingsScope.UserSettings);

            if (!userSettingsStore.CollectionExists(SettingsCollectionName))
            {
                userSettingsStore.CreateCollection(SettingsCollectionName);
            }

            var options = new JiraOptions()
            {
                MaxIssuesPerRequest = this.MaxIssuesPerRequest,
                JiraServers = this.JiraServers,
                Theme = this.Theme,
                OpenIssueMode = this.OpenIssueMode,
                EnableRequestTracing = this.EnableRequestTracing
            };
            var settingString = JsonConvert.SerializeObject(options, serializerSettings);

            userSettingsStore.SetString(
                SettingsCollectionName,
                SettingsPropertyName,
                settingString);

            VSJiraPackage.Services.VisualStudioServices.NotifyOptionsChanged(options);
        }

        public override void LoadSettingsFromStorage()
        {
            base.LoadSettingsFromStorage();

            var settingsManager = new ShellSettingsManager(ServiceProvider.GlobalProvider);
            var userSettingsStore = settingsManager.GetWritableSettingsStore(SettingsScope.UserSettings);

            if (userSettingsStore.PropertyExists(SettingsCollectionName, SettingsPropertyName))
            {
                var settingString = userSettingsStore.GetString(SettingsCollectionName, SettingsPropertyName);
                var jiraOptions = JsonConvert.DeserializeObject<JiraOptions>(settingString, serializerSettings);
                this.JiraServers = jiraOptions.JiraServers;
                this.MaxIssuesPerRequest = jiraOptions.MaxIssuesPerRequest;
                this.Theme = jiraOptions.Theme;
                this.OpenIssueMode = jiraOptions.OpenIssueMode;
                this.EnableRequestTracing = jiraOptions.EnableRequestTracing;
            }
        }
    }
}
