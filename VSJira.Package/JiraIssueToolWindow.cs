﻿using Microsoft.VisualStudio.Shell;
using System;
using System.Runtime.InteropServices;
using VSJira.Core;
using VSJira.Core.UI;

namespace VSJira.Package
{
    [Guid(GuidList.guidJiraIssueToolWindow)]
    public class JiraIssueToolWindow : ToolWindowPane
    {
        private readonly JiraIssueUserControlViewModel _viewModel;
        private readonly JiraIssueUserControl _content;

        public JiraIssueToolWindow() :
            base(null)
        {
            this.BitmapResourceID = 301;
            this.BitmapIndex = 1;

            _viewModel = new JiraIssueUserControlViewModel();
            _content = new JiraIssueUserControl(_viewModel);
            base.Content = _content;
        }

        public void Initialize(JiraIssueViewModel issue, VSJiraServices services)
        {
            base.Caption = issue.Key;
            _viewModel.Initialize(issue, services);
        }

        protected override void Dispose(bool disposing)
        {
            if (_viewModel != null)
            {
                _content.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
