﻿using EnvDTE;
using System;
using System.Linq;
using VSJira.Core;

namespace VSJira.Package
{
    public class VisualStudioServices : IVisualStudioServices
    {
        private readonly DTE _env;
        private readonly VSJiraPackage _package;

        public event EventHandler<OptionsChangedEventArgs> OptionsChanged;

        public VisualStudioServices(DTE environment, VSJiraPackage package)
        {
            this._env = environment;
            this._package = package;
        }

        public void ShowOptionsPage()
        {
            this._package.ShowOptionPage(typeof(JiraOptionsDialogPage));
        }

        public IJiraOptions GetConfigurationOptions()
        {
            var result = new JiraOptions();
            var properties = this._env.get_Properties(JiraOptionsDialogPage.CategoryName, JiraOptionsDialogPage.PageName);

            if (properties != null)
            {
                if (TryGetProperty(properties, "JiraServers", out object[] serverObjects))
                {
                    result.JiraServers = serverObjects.Cast<JiraServerInfo>().ToArray();
                }

                if (TryGetProperty(properties, "MaxIssuesPerRequest", out int maxIssuesPerRequest))
                {
                    result.MaxIssuesPerRequest = maxIssuesPerRequest;
                }

                if (TryGetProperty(properties, "EnableRequestTracing", out bool enableRequestTracing))
                {
                    result.EnableRequestTracing = enableRequestTracing;
                }

                VSJiraOpenIssueMode openIssueMode = VSJiraOpenIssueMode.InBrowser;
                if (TryGetProperty(properties, "OpenIssueModeHidden", out string openIssueMethodStr) && Enum.TryParse<VSJiraOpenIssueMode>(openIssueMethodStr, out openIssueMode))
                {
                    result.OpenIssueMode = openIssueMode;
                }

                VSJiraTheme themeEnum = VSJiraTheme.Light;
                if (TryGetProperty(properties, "ThemeHidden", out string themeStr) && Enum.TryParse<VSJiraTheme>(themeStr, out themeEnum))
                {
                    result.Theme = themeEnum;
                }
            }

            return result;
        }

        private bool TryGetProperty<T>(Properties properties, string propertyName, out T value)
        {
            value = default(T);
            try
            {
                value = (T)properties.Item(propertyName).Value;
            }
            catch
            {
                // no-op
            }

            return value != null;
        }

        public void ShowJiraIssueToolWindow(Core.UI.JiraIssueViewModel viewModel, VSJiraServices services)
        {
            this._package.ShowJiraIssueToolWindow(viewModel, services);
        }

        public void NotifyOptionsChanged(IJiraOptions options)
        {
            var args = new OptionsChangedEventArgs(options);
            this.OptionsChanged?.Invoke(this, args);
        }
    }
}
