﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using VSJira.Core;

namespace VSJira.Wpf
{
    public class MockVisualStudioServices : IVisualStudioServices
    {
        public event EventHandler<OptionsChangedEventArgs> OptionsChanged;

        public IJiraOptions GetConfigurationOptions()
        {
            return new JiraOptions()
            {
                Theme = VSJiraTheme.Dark,
                OpenIssueMode = VSJiraOpenIssueMode.InVisualStudio,
                MaxIssuesPerRequest = 100,
                EnableRequestTracing = false,
                JiraServers = new JiraServerInfo[3]
                {
                    new JiraServerInfo("Live", "https://farmas.atlassian.net", "admin"),
                    new JiraServerInfo("Local", "http://galadriel:2990/jira", "admin"),
                    new JiraServerInfo("Test2", "http://foo.bar2", "myuser2"),
                }
            };
        }

        public void ShowOptionsPage()
        {
            MessageBox.Show("Show Options Page");
        }

        public void ShowJiraIssueToolWindow(VSJira.Core.UI.JiraIssueViewModel viewModel, VSJiraServices services)
        {
            var window = new JiraIssueWindow(viewModel, services);
            window.Show();
        }

        public void NotifyOptionsChanged(IJiraOptions options)
        {
            var args = new OptionsChangedEventArgs(options);
            var handler = this.OptionsChanged;

            if (handler != null)
            {
                handler(this, args);
            }
        }
    }
}
