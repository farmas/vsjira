# Change History

** Version 3.4.0 (04/05/2019) **

* Adds ability to capture request traces and report them to user.

** Version 3.3.0 (04/03/2019) **

* Adds support for opening an issue by its key in all windows.

** Version 3.2.0 (07/05/2018) **

* Adds support for setting the server proxy url.

** Version 3.1.0 (01/05/2018) **

* Adds support for updaing 'Fix Versions', 'Affects Versions', 'Components' and 'Labels' fields.

** Version 3.0.0 (12/02/2017) **

* Adds support for VS2017.

** Version 2.7.0 (01/26/2017) **

* Add setting to specify the how to open issue when clicking on keys on issue lists.

** Version 2.6.0 (01/19/2017) **

* Add ability to create work logs on an issue.
* Add ability to list linked issues.
* Drop support for VS2013.

** Version 2.5.0 (01/0/2017) **

* Add ability to list work logs of an issue.
* Add ability to copy error details to clipboard.

** Version 2.4.0 (12/14/2015) **

* Add ability to list and download attachments.

** Version 2.3.0 (12/11/2015) **

* Add ability to create issues from whithin the issues tool window.

** Version 2.2.0 (12/09/2015) **

* Add ability to execute a workflow transition on an issue.

** Version 2.1.0 (12/05/2015) **

* Add ability to view and edit comments on issues.

** Version 2.0.0 (12/02/2015) **

* Add ability to switch between Light and Dark themes. Fixes https://bitbucket.org/farmas/vsjira/issues/6.

** Version 1.3.0 (11/26/2015) **

* Add ability to update JIRA issue fields within the tool window.
- Use current culture to format dates. Fixes https://bitbucket.org/farmas/vsjira/issues/7. 

** Version 1.2.0 (11/25/2015) **

* Add a multi-instance VisualStudio tool window to show read-only information of an issue.