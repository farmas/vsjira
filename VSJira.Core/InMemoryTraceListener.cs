﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSJira.Core
{
    public class InMemoryTraceListener : TraceListener, IDisposable
    {
        private MemoryStream _memoryStream;
        private StreamWriter _streamWriter;
        private TextWriterTraceListener _textWriterTraceListener;

        public InMemoryTraceListener()
        {
            this.Initialize();
        }

        private void Initialize()
        {
            _memoryStream = new MemoryStream();
            _streamWriter = new StreamWriter(_memoryStream);
            _textWriterTraceListener = new TextWriterTraceListener(_streamWriter);
        }

        public override void Write(string message)
        {
            _textWriterTraceListener.Write(message);
        }

        public override void WriteLine(string message)
        {
            _textWriterTraceListener.WriteLine(message);
        }

        public string GetTraces()
        {
            _streamWriter.Flush();

            _memoryStream.Position = 0;
            using (var streamReader = new StreamReader(_memoryStream))
            {
                var result = streamReader.ReadToEnd();

                Initialize();

                return result;
            }
        }

        public new void Dispose()
        {
            _textWriterTraceListener.Dispose();
            _streamWriter.Dispose();
            _memoryStream.Dispose();
            base.Dispose();
        }

    }
}
