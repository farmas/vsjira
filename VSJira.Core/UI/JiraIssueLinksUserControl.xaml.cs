﻿using System.Windows.Controls;

namespace VSJira.Core.UI
{
    /// <summary>
    /// Interaction logic for JiraIssueLinksUserControl.xaml
    /// </summary>
    public partial class JiraIssueLinksUserControl : UserControl
    {
        private readonly JiraIssueLinksUserControlViewModel _viewModel;

        public JiraIssueLinksUserControl(JiraIssueLinksUserControlViewModel viewModel)
        {
            InitializeComponent();

            this.DataContext = _viewModel = viewModel;
        }
    }
}
