﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VSJira.Core.UI
{
    /// <summary>
    /// Interaction logic for OpenIssueDialog.xaml
    /// </summary>
    public partial class OpenIssueDialog : MetroWindow, ICloseableWindow
    {
        public OpenIssueDialog(OpenIssueDialogViewModel viewModel)
        {
            InitializeComponent();

            var theme = viewModel.Services.VisualStudioServices.GetConfigurationOptions().Theme;
            ThemeManager.Apply(this.Resources, theme);
        }
    }
}
