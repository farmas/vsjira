﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSJira.Core.UI
{
    public class JiraBackedViewModel : ViewModelBase
    {
        private Jira _jira;

        public VSJiraServices Services { get; }

        public JiraBackedViewModel(VSJiraServices services)
        {
            this.Services = services;
        }

        public JiraBackedViewModel(VSJiraServices services, Jira jira)
        {
            this.Services = services;
            this._jira = jira;
        }

        public bool IsConnected
        {
            get
            {
                return this._jira != null;
            }
        }

        public Jira Jira
        {
            get
            {
                return _jira;
            }
            set
            {
                _jira = value;
                OnPropertyChanged("IsConnected");
            }
        }
    }
}
