﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSJira.Core.UI
{
    public class OpenIssueDialogViewModel : JiraBackedViewModel
    {
        private string _status;
        private string _issueKey;
        private bool _isIdle;
        public DelegateCommand<ICloseableWindow> OpenCommand { get; private set; }
        public bool IsIssueKeyFocused { get; private set; } = true;

        public OpenIssueDialogViewModel(Jira jira, VSJiraServices services)
            : base(services, jira)
        {
            this.OpenCommand = new DelegateCommand<ICloseableWindow>(async (window) => await this.OpenIssueAsync(window));
            this.Status = "Ready";
            this.IsIdle = true;
        }

        public bool IsIdle
        {
            get
            {
                return _isIdle;
            }
            set
            {
                _isIdle = value;
                OnPropertyChanged("IsIdle");
            }
        }

        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
                OnPropertyChanged("Status");
            }
        }

        public string IssueKey
        {
            get
            {
                return _issueKey;
            }
            set
            {
                _issueKey = value;
                OnPropertyChanged("IssueKey");
            }
        }

        private async Task OpenIssueAsync(ICloseableWindow window)
        {
            Status = "";

            if (String.IsNullOrEmpty(this.IssueKey))
            {
                return;
            }

            Status = "Loading Issue...";
            var openIssueMethod = this.Services.VisualStudioServices.GetConfigurationOptions().OpenIssueMode;

            if (openIssueMethod == VSJiraOpenIssueMode.InBrowser)
            {
                OpenIssueInBrowser(Jira.Url, IssueKey);
                window.Close();
            }
            else
            {
                this.IsIdle = false;
                var result = await TryOpenIssueToolWindowAsync(Jira, Services, IssueKey);
                this.IsIdle = true;

                if (result)
                {
                    window.Close();
                }
                else
                {
                    Status = "Issue not found.";
                }
            }
        }

        private static async Task<bool> TryOpenIssueToolWindowAsync(Jira jira, VSJiraServices services, string issueKey)
        {
            var issues = await jira.Issues.GetIssuesAsync(issueKey);

            if (issues.TryGetValue(issueKey, out Issue issue))
            {
                var issueVM = new JiraIssueViewModel(issue, services);
                services.VisualStudioServices.ShowJiraIssueToolWindow(issueVM, services);
                return true;
            }

            return false;
        }

        private static void OpenIssueInBrowser(string jiraUrl, string issueKey)
        {
            var baseUri = new Uri(jiraUrl);
            var issueUrl = new Uri(baseUri, "browse/" + issueKey);
            System.Diagnostics.Process.Start(issueUrl.ToString());
        }
    }
}
