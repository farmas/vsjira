﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSJira.Core
{
    public interface IJiraOptions
    {
        JiraServerInfo[] JiraServers { get; }
        int MaxIssuesPerRequest { get; }
        VSJiraTheme Theme { get; }
        VSJiraOpenIssueMode OpenIssueMode { get; set; }
        bool EnableRequestTracing { get; }
    }
}
