﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlassian.Jira.Linq;
using System.Threading;
using Atlassian.Jira.Remote;
using Newtonsoft.Json.Linq;
using System.Collections.ObjectModel;

namespace VSJira.Core
{
    public class TestIssueService : IIssueService, IIssueLinkService
    {
        private readonly int _totalIssues;
        private readonly Jira _jira;
        private readonly Random _random;

        private IList<RemoteIssue> _remoteIssues;
        private IList<Comment> _comments;
        private IList<Worklog> _worklogs;

        private string[] _jiraActions = new string[2] { "Resolve", "Close" };
        private IList<IssueLinkType> _issueLinkTypes = new List<IssueLinkType>()
        {
            new IssueLinkType("1", "Duplicated", "is duplicated by", "duplicates"),
            new IssueLinkType("2", "Caused", "is caused by", "causes"),
            new IssueLinkType("3", "Related", "is related by", "relates")
        };

        #region Ipsum Lorem
        private const string IpsumLorem = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque finibus odio vel feugiat lacinia. Nulla eu magna sit amet arcu eleifend facilisis. Cras vel nulla bibendum, tristique mi id, suscipit leo. Phasellus magna metus, aliquet id felis id, rhoncus gravida orci. Cras eu dui vitae metus rhoncus aliquet eget ac magna. Integer rhoncus, libero ac ornare venenatis, sapien magna sodales sapien, eget rutrum nulla ante at quam. Cras ac laoreet purus. Mauris vulputate maximus ex et pulvinar. Aliquam quis blandit libero, id cursus nibh. In eu porta sem, nec dignissim nisi.

Suspendisse vel ante gravida, sagittis tortor quis, pellentesque dui. Proin placerat mi ac congue convallis. Duis et arcu ligula. Sed vel auctor neque. Donec in nunc eu diam elementum dignissim eu nec mauris. Nulla rutrum mi ante, a ullamcorper turpis congue ullamcorper. Suspendisse ac vehicula neque, ac aliquam augue. Phasellus condimentum dapibus nulla, convallis pharetra ex. Integer lorem quam, tristique ac sodales et, eleifend id dolor. In semper turpis justo, fringilla semper lorem finibus vitae. Curabitur aliquet tellus vitae lectus posuere, vitae posuere ligula consectetur. Pellentesque sed ante erat.

Integer dapibus est ut ultrices dictum. Proin vitae est mattis, volutpat orci id, hendrerit risus. Nulla ac urna lobortis, ornare quam in, imperdiet ante. Aenean facilisis rutrum libero, at placerat mi. Praesent maximus convallis pulvinar. Sed sodales non ipsum quis convallis. Curabitur posuere molestie est, sed bibendum diam tincidunt eu. Praesent vel quam orci. Nunc fringilla ipsum tempus, gravida arcu at, vehicula lectus. Mauris sollicitudin erat a tincidunt fringilla. Aenean in ex cursus, pretium ligula semper, fringilla libero.";
        #endregion

        #region PagedQueryResult
        public class PagedQueryResult<T> : IPagedQueryResult<T>
        {
            public IEnumerable<T> Enumerable { get; set; }
            public int ItemsPerPage { get; set; }
            public int StartAt { get; set; }
            public int TotalItems { get; set; }

            public IEnumerator<T> GetEnumerator()
            {
                return Enumerable.GetEnumerator();
            }

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return Enumerable.GetEnumerator();
            }
        }
        #endregion

        public TestIssueService(Jira jira, int totalIssues)
        {
            this._jira = jira;
            this._totalIssues = totalIssues;
            this._random = new Random();

            var random = new Random();
            var priorities = jira.Priorities.GetPrioritiesAsync().Result.Select(p => new RemotePriority() { id = p.Id, name = p.Name }).ToArray();
            var statuses = jira.Statuses.GetStatusesAsync().Result.Select(p => new RemoteStatus() { id = p.Id, name = p.Name }).ToArray();
            var types = jira.IssueTypes.GetIssueTypesAsync().Result.Select(p => new RemoteIssueType() { id = p.Id, name = p.Name }).ToArray();

            _worklogs = Enumerable.Range(0, 5).Select(i =>
                 new Worklog($"{i + 1} hours", DateTime.Now, $"My Comment - {i}")
                 {
                     Author = "Federico Silva"
                 }
            ).ToList();

            _comments = Enumerable.Range(0, _totalIssues)
                .Select(i => new RemoteComment() { author = "author", body = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque finibus odio vel feugiat lacinia.", created = DateTime.Now })
                .Select(remoteComment => new Comment(remoteComment))
                .ToList();

            _remoteIssues = Enumerable.Range(0, _totalIssues)
                .Select(i => new RemoteIssue()
                {
                    id = String.Format("{0}", i),
                    assignee = "farmas",
                    created = DateTime.Now,
                    key = String.Format("TST-{0}", i),
                    priority = priorities[random.Next(0, priorities.Length)],
                    status = statuses[random.Next(0, statuses.Length)],
                    summary = String.Format("My Summary {0}", i),
                    type = types[random.Next(0, types.Length)],
                    description = String.Format("{0}: {1}", i, IpsumLorem)
                })
                .ToList();
        }

        public JiraQueryable<Issue> Queryable
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool ValidateQuery { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int MaxIssuesPerRequest { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public Task AddAttachmentsAsync(string issueKey, UploadAttachmentInfo[] attachments, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<Comment> AddCommentAsync(string issueKey, Comment comment, CancellationToken token = default(CancellationToken))
        {
            var remoteComment = new RemoteComment() { author = comment.Author, body = comment.Body };
            var newComment = new Comment(remoteComment);

            this._comments.Add(newComment);

            return Task.FromResult(newComment);
        }

        public Task AddWatcherAsync(string issueKey, string username, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<Worklog> AddWorklogAsync(string issueKey, Worklog worklog, WorklogStrategy worklogStrategy = WorklogStrategy.AutoAdjustRemainingEstimate, string newEstimate = null, CancellationToken token = default(CancellationToken))
        {
            _worklogs.Add(worklog);
            return Task.FromResult(worklog);
        }

        public Task<string> CreateIssueAsync(Issue issue, CancellationToken token = default(CancellationToken))
        {
            var remoteIssue = issue.ToRemote();
            remoteIssue.key = remoteIssue.id = _remoteIssues.Count().ToString();

            this._remoteIssues.Add(remoteIssue);
            return Task.FromResult(remoteIssue.id);
        }

        public Task DeleteAttachmentAsync(string issueKey, string attachmentId, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task DeleteIssueAsync(string issueKey, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task DeleteWatcherAsync(string issueKey, string username, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task DeleteWorklogAsync(string issueKey, string worklogId, WorklogStrategy worklogStrategy = WorklogStrategy.AutoAdjustRemainingEstimate, string newEstimate = null, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task ExecuteWorkflowActionAsync(Issue issue, string actionName, WorkflowTransitionUpdates updates, CancellationToken token = default(CancellationToken))
        {
            return Task.FromResult(true);
        }

        public Task<IEnumerable<JiraNamedEntity>> GetActionsAsync(string issueKey, CancellationToken token = default(CancellationToken))
        {
            return Task.FromResult(Enumerable.Range(0, 2).Select(i =>
                new JiraNamedEntity(new RemoteNamedObject()
                {
                    id = i.ToString(),
                    name = _jiraActions[i]
                })));
        }

        public Task<IEnumerable<Attachment>> GetAttachmentsAsync(string issueKey, CancellationToken token = default(CancellationToken))
        {
            return Task.FromResult(Enumerable.Range(0, 2).Select(i =>
                new Attachment(_jira, new RemoteAttachment()
                {
                    author = "admin",
                    created = DateTime.Now,
                    filename = String.Format("file{0}.txt", i),
                    filesize = 1000
                })));
        }

        public Task<IEnumerable<IssueChangeLog>> GetChangeLogsAsync(string issueKey, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Comment>> GetCommentsAsync(string issueKey, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<IDictionary<string, IssueFieldEditMetadata>> GetFieldsEditMetadataAsync(string issueKey, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<Issue> GetIssueAsync(string issueKey, CancellationToken token = default(CancellationToken))
        {
            return Task.FromResult(this._remoteIssues.First(issue => issue.key == issueKey).ToLocal(_jira));
        }

        public Task<IDictionary<string, Issue>> GetIssuesAsync(IEnumerable<string> issueKeys, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<IPagedQueryResult<Issue>> GetIssuesFromJqlAsync(string jql, int? maxIssues = default(int?), int startAt = 0, CancellationToken token = default(CancellationToken))
        {
            var issues = _remoteIssues.Select(i => i.ToLocal(_jira)).ToList();
            SortIssuesList(issues, jql);

            var result = new PagedQueryResult<Issue>()
            {
                ItemsPerPage = maxIssues ?? 100,
                StartAt = startAt,
                TotalItems = _remoteIssues.Count(),
                Enumerable = issues.Skip(startAt).Take(maxIssues ?? 100)
            };

            return Task.FromResult<IPagedQueryResult<Issue>>(result);
        }

        private void SortIssuesList(List<Issue> issues, string jql)
        {
            if (jql.IndexOf("order by", StringComparison.OrdinalIgnoreCase) < 0)
            {
                return;
            }

            var sortArray = jql.Split(' ').Reverse().Take(2).ToArray();
            var sortDirection = sortArray[0];
            var sortField = sortArray[1];

            issues.Sort((x, y) =>
            {
                var property = typeof(Issue).GetProperty(sortField);
                var valX = property.GetValue(x).ToString();
                var valY = property.GetValue(y).ToString();
                var comparison = valX.CompareTo(valY);

                if (sortDirection == "desc")
                {
                    comparison *= -1;
                }

                return comparison;
            });
        }

        public Task<string[]> GetLabelsAsync(string issueKey, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<IPagedQueryResult<Comment>> GetPagedCommentsAsync(string issueKey, int? maxComments = default(int?), int startAt = 0, CancellationToken token = default(CancellationToken))
        {
            var result = new PagedQueryResult<Comment>()
            {
                TotalItems = _comments.Count,
                ItemsPerPage = maxComments.Value,
                StartAt = startAt,
                Enumerable = _comments.Skip(startAt).Take(maxComments.Value)
            };

            return Task.FromResult<IPagedQueryResult<Comment>>(result);
        }

        public Task<IPagedQueryResult<Issue>> GetSubTasksAsync(string issueKey, int? maxIssues = default(int?), int startAt = 0, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<IssueTimeTrackingData> GetTimeTrackingDataAsync(string issueKey, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<JiraUser>> GetWatchersAsync(string issueKey, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<Worklog> GetWorklogAsync(string issueKey, string worklogId, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Worklog>> GetWorklogsAsync(string issueKey, CancellationToken token = default(CancellationToken))
        {
            return Task.FromResult(_worklogs.AsEnumerable());
        }

        public Task SetLabelsAsync(string issueKey, string[] labels, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task UpdateIssueAsync(Issue issue, CancellationToken token = default(CancellationToken))
        {
            return Task.FromResult(issue);
        }

        public Task<IEnumerable<IssueLinkType>> GetLinkTypesAsync(CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task CreateLinkAsync(string outwardIssueKey, string inwardIssueKey, string linkName, string comment, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IssueLink>> GetLinksForIssueAsync(string issueKey, CancellationToken token)
        {
            var issue = this._remoteIssues.First(i => i.key == issueKey).ToLocal(_jira);
            var issueLinks = new List<IssueLink>();

            foreach (var i in Enumerable.Range(0, 5))
            {
                issueLinks.Add(new IssueLink(
                    _issueLinkTypes[_random.Next(_issueLinkTypes.Count)],
                    issue,
                    _remoteIssues[_random.Next(_remoteIssues.Count)].ToLocal(_jira)
                ));

                issueLinks.Add(new IssueLink(
                    _issueLinkTypes[_random.Next(_issueLinkTypes.Count)],
                    _remoteIssues[_random.Next(_remoteIssues.Count)].ToLocal(_jira),
                    issue
                ));
            }

            return Task.FromResult(issueLinks.AsEnumerable());
        }

        public Task<IDictionary<string, Issue>> GetIssuesAsync(params string[] issueKeys)
        {
            var issues = _remoteIssues.Where(i => issueKeys.Contains(i.key)).Select(i => i.ToLocal(_jira)).ToDictionary(i => i.Key.Value);
            return Task.FromResult<IDictionary<string, Issue>>(issues);
        }

        public Task UpdateIssueAsync(Issue issue, IssueUpdateOptions options, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<IPagedQueryResult<Issue>> GetIssuesFromJqlAsync(IssueSearchOptions options, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Comment>> GetCommentsAsync(string issueKey, CommentQueryOptions options, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task DeleteCommentAsync(string issueKey, string commentId, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<Comment> UpdateCommentAsync(string issueKey, Comment comment, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        Task<IEnumerable<IssueTransition>> IIssueService.GetActionsAsync(string issueKey, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        public Task AssignIssueAsync(string issueKey, string assignee, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<string>> GetPropertyKeysAsync(string issueKey, CancellationToken token = default)
        {
            throw new NotImplementedException();
        }

        public Task<ReadOnlyDictionary<string, JToken>> GetPropertiesAsync(string issueKey, IEnumerable<string> propertyKeys, CancellationToken token = default)
        {
            throw new NotImplementedException();
        }

        public Task SetPropertyAsync(string issueKey, string propertyKey, JToken obj, CancellationToken token = default)
        {
            throw new NotImplementedException();
        }

        public Task DeletePropertyAsync(string issueKey, string propertyKey, CancellationToken token = default)
        {
            throw new NotImplementedException();
        }
    }
}
